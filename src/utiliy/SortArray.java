package utiliy;

import models.Product;

public class SortArray {

    // Funciones del método de burbuja
    public static int[] bubbleMethod(int[] array) {
        do {
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    int temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
        } while (isNotArraySorted(array));
        return array;
    }

    public static int[] bubbleMethodDesc(int[] array) {
        do {
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] < array[i + 1]) {
                    int temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
        } while (isNotArraySortedDesc(array));
        return array;
    }

    private static boolean isNotArraySorted(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                return true;
            }
        }
        return false;
    }

    private static boolean isNotArraySortedDesc(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] < array[i + 1]) {
                return true;
            }
        }
        return false;
    }

    // Funciones del método de selección
    public static int[] selectionMethod(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int[] maxNumber = getMaxNumber(array, i);

            int temp = array[i];
            array[i] = maxNumber[0];
            array[maxNumber[1]] = temp;
        }
        return array;
    }

    private static int[] getMaxNumber(int[] array, int initialNumber) {
        int[] maxNumber = new int[2];
        maxNumber[0] = array[initialNumber];
        maxNumber[1] = initialNumber;
        for (int i = initialNumber; i < array.length; i++) {
            if (maxNumber[0] > array[i]) {
                maxNumber[0] = array[i];
                maxNumber[1] = i;
            }
        }
        return maxNumber;
    }

    // Función del método de inserción
    public static int[] insertionMethod(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int aux = array[i];
            for (int j = i - 1; j >= 0; j--) {
                if (aux < array[j]) {
                    array[j + 1] = array[j];
                    array[j] = aux;
                }
            }
        }
        return array;
    }

    public static Product[] insertionMethodProducts(Product[] array) {
        for (int i = 1; i < array.length; i++) {
            Product aux = array[i];
            for (int j = i - 1; j >= 0; j--) {
                assert array[j] != null;
                if (aux.getPrice() < array[j].getPrice()) {
                    array[j + 1] = array[j];
                    array[j] = aux;
                }
            }
        }
        return array;
    }
}
